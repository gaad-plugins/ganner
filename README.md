###Automatic testing
Tests are powered by Codeception framework. PHP 7.x needs to be installed on host machine.

##Running API tests
To run tests:
1. change dir to ganner directory
2. execute `./vendor/bin/codecept run api`

##CLI with PHP-XDEBUG
https://blog.rabin.io/sysadmin/how-to-debug-php-script-from-the-cli-with-php-xdebug


##List with Criteria
Based on: https://www.doctrine-project.org/projects/doctrine-collections/en/1.6/expressions.html
headers should look like:
Criteria-<operator>-<index>
Criteria-and-1:gt('created', new Datetime('2020-03-18 06:34:40'))
Criteria-and-2:lt('created', '2020-03-18 06:34:44')