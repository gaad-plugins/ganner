<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

define('DB_NAME', 'wpdocker');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'mysql');
define('ENV', 'dev');
define('__GANNER_DIR__', __DIR__ . "/../");

$oGAEntityManager = EntityManager::create([
    'driver' => 'pdo_mysql',
    'host' => DB_HOST,
    'user' => DB_USER,
    'password' => DB_PASSWORD,
    'dbname' => DB_NAME
], Setup::createAnnotationMetadataConfiguration(array(__GANNER_DIR__ . "inc/class/Entity"), ENV === 'dev' ?? false, null, null, false));

$oGAEntityManager
    ->getConnection()
    ->getDatabasePlatform()
    ->registerDoctrineTypeMapping('enum', 'string');

return ConsoleRunner::createHelperSet($oGAEntityManager);