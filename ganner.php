<?php
/*
 Plugin Name: Ganner
 Plugin URI:
 Description: GAAD Planner.
 Version: 0.1
 Author: Bartek GAAD Sosnowski
 Author URI:
 Text Domain: ganner
 */
define('__GANNER_DIR__', __DIR__);
file_exists(__DIR__ . "/vendor/autoload.php") ? require_once __DIR__ . "/vendor/autoload.php" : null;
$wp_dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../../../'); // wp root
$wp_dotenv->load();
define('ENV', getenv('PAGE_ENV'));
include_once __GANNER_DIR__ . "/inc/bootstrap.php";
