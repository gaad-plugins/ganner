<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\TaskMoveToBucketEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(TaskMoveToBucketEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oBucketsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
$oBucket = $oBucketsRepo->findBy(["id" => $aHeaders['Target']]);
if (!empty($oBucket)) {
    $oBucket[0]->addTask($aHeaders['Id']);
}