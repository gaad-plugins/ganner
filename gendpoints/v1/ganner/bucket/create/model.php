<?php

use Gaad\Ganner\Entity\Bucket as Bucket;
use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\BucketCreateEndpointValidator;

global $oGAEntityManager;

$aHeaders = GEndpoint::getAllHeaders(BucketCreateEndpointValidator::aHeaders, $this->getAHeaders());
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oBucket = new Bucket($aHeaders);
$oGAEntityManager->persist($oBucket);
$oGAEntityManager->flush();

$this->oModel->addData($oBucket->getId(), "created_id");