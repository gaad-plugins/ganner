<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\BucketRemoveEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(BucketRemoveEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oBucketsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
$oBucket = $oBucketsRepo->findBy(["id" => $aHeaders['Id']]);
if (!empty($oBucket)) {
    $deleted_id = $oBucket[0]->remove();
    $this->oModel->addData($deleted_id, "removed_id");
}