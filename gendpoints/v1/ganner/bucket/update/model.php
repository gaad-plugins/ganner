<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\BucketCreateEndpointValidator;
use Gaad\Gendpoints\Router\Validation\BucketRemoveEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(array_merge(BucketCreateEndpointValidator::aHeaders, BucketRemoveEndpointValidator::aHeaders));
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oBucketsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
$oBucket = $oBucketsRepo->findBy(["id" => $aHeaders['Id']]);
if (!empty($oBucket)) {
    $oBucket[0]->update($aHeaders);
    $this->oModel->addData($aHeaders['Id'], "updated_id");
}