<?php
global $oGAEntityManager;

use Gaad\Ganner\Entity\Comment;
use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\CommentCreateEndpointValidator;

$aHeaders = GEndpoint::getAllHeaders(CommentCreateEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oComment = new Comment($aHeaders);
$oGAEntityManager->persist($oComment);
$oGAEntityManager->flush();

$this->oModel->addData($oComment->getId(), "created_id");