<?php

global $oGAEntityManager;

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\CommentCreateEndpointValidator;

$aHeaders = GEndpoint::getAllHeaders(CommentCreateEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oCommentsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Comment");
$oComment = $oCommentsRepo->findBy(["id" => $aHeaders['Id']]);
if (!empty($oComment)) {
    $deleted_id = $oComment[0]->remove();
    $this->oModel->addData($deleted_id, "removed_id");
}
