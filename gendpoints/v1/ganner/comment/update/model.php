<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\CommentCreateEndpointValidator;
use Gaad\Gendpoints\Router\Validation\CommentRemoveEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(array_merge(CommentCreateEndpointValidator::aHeaders, CommentRemoveEndpointValidator::aHeaders));
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oCommentsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Comment");
$oComment = $oCommentsRepo->findBy(["id" => $aHeaders['Id']]);
if (!empty($oComment)) {
    $oComment[0]->update($aHeaders);
    $this->oModel->addData($aHeaders['Id'], "updated_id");
}