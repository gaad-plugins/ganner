<?php
/**
 * Add existing bucket to the project
 */

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\ProjectAddBucketEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(ProjectAddBucketEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
$oProject = $oProjectsRepo->findBy(["id" => $aHeaders['Project']]);
if (!empty($oProject)) {
    $oProject[0]->addBucket($aHeaders['Id']);
}