<?php

use Gaad\Ganner\Entity\Project as Project;
use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\ProjectAddTaskEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(ProjectAddTaskEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
$oProject = $oProjectsRepo->findBy(["id" => $aHeaders['Target']]);
if (!empty($oProject)) {
    $sBucket = (int)$aHeaders['Bucket'] > 0 ? $aHeaders['Bucket'] : null;
    $oProject[0]->addTask($aHeaders['Id'], $sBucket);
}