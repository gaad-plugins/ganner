<?php

use Gaad\Ganner\Entity\Project as Project;
use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\ProjectCreateEndpointValidator;

global $oGAEntityManager;

$aHeaders = GEndpoint::getAllHeaders(ProjectCreateEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oProject = new Project($aHeaders);
$oGAEntityManager->persist($oProject);
$oGAEntityManager->flush();

$this->oModel->addData($oProject->getId(), "created_id");