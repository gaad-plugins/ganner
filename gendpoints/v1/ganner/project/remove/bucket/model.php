<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\ProjectRemoveBucketEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(ProjectRemoveBucketEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
$oProject = $oProjectsRepo->findBy(["id" => $aHeaders['Target']]);
if (!empty($oProject)) {
    $target = (int)$aHeaders['Targetbucket'] > 0 ? $aHeaders['Targetbucket'] : null;
    $oProject[0]->removeBucket((int)$aHeaders['Id'], $target);
}