<?php

use Gaad\Ganner\Entity\Project as Project;
use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\ProjectRemoveTaskEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(ProjectRemoveTaskEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
$oProject = $oProjectsRepo->findBy(["id" => $aHeaders['Target']]);
if (!empty($oProject)) {
    $oProject[0]->removeTask($aHeaders['Id']);
}