<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\ProjectCreateEndpointValidator;
use Gaad\Gendpoints\Router\Validation\ProjectRemoveEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(array_merge(ProjectCreateEndpointValidator::aHeaders, ProjectRemoveEndpointValidator::aHeaders));
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
$oProject = $oProjectsRepo->findBy(["id" => $aHeaders['Id']]);
if (!empty($oProject)) {
    $oProject[0]->update($aHeaders);
    $this->oModel->addData($aHeaders['Id'], "updated_id");
}