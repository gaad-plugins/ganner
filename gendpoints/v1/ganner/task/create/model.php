<?php

use Gaad\Ganner\Entity\Task;
use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\TaskCreateEndpointValidator;

global $oGAEntityManager;

$aHeaders = GEndpoint::getAllHeaders(TaskCreateEndpointValidator::aHeaders, $this->getAHeaders());
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oTask = new Task($aHeaders);
$oGAEntityManager->persist($oTask);
$oGAEntityManager->flush();

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($oTask->getId(), "created_id");