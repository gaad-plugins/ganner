<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\TaskListEndpointValidator;
use Gaad\Ganner\Handlers\EntityListWithCriteria;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(
    array_merge(\Gaad\Gendpoints\Router\Validation\GenericEndpointValidator::aHeaders, TaskListEndpointValidator::aHeaders),
    $this->getAHeaders());
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oList = new EntityListWithCriteria("\Gaad\Ganner\Entity\Task", $aHeaders);
$oCriteria = $oList->getOCriteria();
//@TODO add criteria here*/

$aTasks = $oList->toArray();

$this->oModel->addData(count($aTasks), "length");
$this->oModel->addData($oList->getIPage(), "page");
$this->oModel->addData($oList->getMaxPages(), "maxPages");
$this->oModel->addData($aTasks, "list");