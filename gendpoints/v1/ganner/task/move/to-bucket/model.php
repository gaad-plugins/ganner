<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\TaskMoveToBucketEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(TaskMoveToBucketEndpointValidator::aHeaders);
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
$oTask = $oTasksRepo->findBy(["id" => $aHeaders['Id']]);
if (!empty($oTask)) {
    $oTask[0]->moveToBucket($aHeaders['Target']);
}