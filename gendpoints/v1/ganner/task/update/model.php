<?php

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Router\Validation\TaskCreateEndpointValidator;
use Gaad\Gendpoints\Router\Validation\TaskRemoveEndpointValidator;

global $oGAEntityManager;
$aHeaders = GEndpoint::getAllHeaders(array_merge(TaskCreateEndpointValidator::aHeaders, TaskRemoveEndpointValidator::aHeaders));
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
$oTask = $oTasksRepo->findBy(["id" => $aHeaders['Id']]);
if (!empty($oTask)) {
    $oTask[0]->update($aHeaders);
    $this->oModel->addData($aHeaders['Id'], "updated_id");
}