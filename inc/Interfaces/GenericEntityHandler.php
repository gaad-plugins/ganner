<?php

namespace Gaad\Ganer\Interfaces;


if (!interface_exists(__NAMESPACE__ . '\GenericEntityHandler')) {
    interface GenericEntityHandler
    {

        public function getClassName(): string;

        public function update();


    }
}

