<?php

namespace Gaad\Gunner;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

if (!defined('WP_CLI')) {
    \add_filter('ge-config-directories', '\Gaad\Gunner\Core\Filters\configDirectories');
    \add_filter('ge-endpoints-directories', '\Gaad\Gunner\Core\Filters\endpointsDirectories');
}


/*
 * Doctrine Entity Manager
 */
$GLOBALS['oGAEntityManager'] = EntityManager::create(
    ['driver' => 'pdo_mysql', 'host' => \DB_HOST, 'user' => \DB_USER, 'password' => \DB_PASSWORD, 'dbname' => \DB_NAME],
    Setup::createAnnotationMetadataConfiguration([__GANNER_DIR__ . "/config"], ENV === 'dev' ?? false, null, null, false)
);

$GLOBALS['oGAEntityManager']
    ->getConnection()
    ->getDatabasePlatform()
    ->registerDoctrineTypeMapping('enum', 'string');


\add_filter('init', '\Gaad\Gunner\Core\Filters\userRolesAdd');
