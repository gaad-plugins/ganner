<?php

namespace Gaad\Ganner\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gaad\Ganer\Interfaces\GenericEntityHandler;

/**
 * Gaad\Gendpoints\Entity\Bucket
 *
 * @ORM\Entity
 * @ORM\Table(name="ganner_bucket")
 **/
class Bucket extends GenericEntity implements GenericEntityHandler
{
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue * */
    private $id;

    /** @ORM\Column(type="datetime") * */
    private $created;

    /** @ORM\Column(length=50) * */
    private $name = "New bucket";

    /** @ORM\Column(type="integer") * */
    private $project_id;

    /** @ORM\Column(type="string", columnDefinition="ENUM('active', 'blocked', 'trashed') NOT NULL DEFAULT 'active'") */
    private $status = 'active';

    /** @ORM\Column(type="integer") * */
    private $ord;


    /**
     * Project constructor.
     * @param array $aData
     */
    public function __construct(array $aData = [])
    {
        parent::__construct($aData);
        parent::applyData($aData);
        $this->setCreated();
        is_null($this->getOrd()) ? $this->setMaxOrder() : false;
    }


    public function update(array $aData = [])
    {
        global $oGAEntityManager;
        parent::applyData($aData);
        $oGAEntityManager->persist($this);
        $oGAEntityManager->flush();
    }

    /**
     * @param bool $bSoftRemove
     * @param int|null $iSentToTargetBucket
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(bool $bSoftRemove = false, int $iSentToTargetBucket = NULL)
    {
        global $oGAEntityManager;
        $deleted_id = $this->getId();
        !$bSoftRemove ? $this->removeTasks() : $this->removeBucketFromTasks($iSentToTargetBucket);

        $this->trash();

        return $deleted_id;
    }

    /**
     * Returns entity as array
     *
     * @param string|null $sClassName
     * @return array
     */
    public function toArray(string $sClassName = NULL): array
    {
        return parent::toArray(__CLASS__);
    }

    /**
     *
     */
    public function addTask(int $iTaskId)
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTask = $oTasksRepo->findBy(["id" => $iTaskId]);
        if (!empty($aTask)) {
            $aTask[0]->moveToBucket($this->getId());
        }
    }

    /**
     *
     */
    public function removeTask(int $iTaskId)
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTask = $oTasksRepo->findBy(["id" => $iTaskId]);
        if (!empty($aTask)) {
            $aTask[0]->moveToBucket(null);
        }
    }

    /**
     *
     */
    public function removeTasks()
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        foreach ($oTasksRepo->findBy(["bucket_id" => $this->getId()]) as $i => $task) {
            $task->remove();
        }
    }

    public function trash()
    {
        $this->setStatus('trashed');
        $this->update();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = (int)$id;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    private function setCreated(): void
    {
        $this->created = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project_id;
    }

    /**
     * @param mixed $project_id
     */
    public function setProject($project_id): void
    {
        $this->project_id = (int)$project_id;
    }

    /**
     * @return mixed
     */
    public function getOrd()
    {
        return $this->ord;
    }

    /**
     * @param mixed $ord
     */
    public function setOrd($ord): void
    {
        $this->ord = $ord;
    }

    /**
     * Returns class name
     *
     * @return string
     */
    public function getClassName(): string
    {
        return self::class;
    }

    /**
     * Remove all tasks from bucket with option to move them to another one
     *
     * @param int|null $iSentToTargetBucket
     */
    private function removeBucketFromTasks(int $iSentToTargetBucket = NULL)
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTask = $oTasksRepo->findBy(["bucket_id" => $this->getId()]);
        if (!empty($aTask)) {
            foreach ($aTask as $oTask) {
                $oTask->moveToBucket($iSentToTargetBucket);
            }
        }
    }

    public function setMaxOrder()
    {
        global $oGAEntityManager;
        $aQuery = [
            "SELECT max(b.ord) as maxOrder FROM {$this->getClassName()} b",
            "WHERE b.project_id = {$this->getProject()}"
        ];
        $aResult = $oGAEntityManager->createQuery(implode(" ", $aQuery))->getResult();
        $this->setOrd((int)$aResult[0]['maxOrder'] + 1);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
}