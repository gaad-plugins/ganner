<?php

namespace Gaad\Ganner\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gaad\Ganer\Interfaces\GenericEntityHandler;

/**
 * Gaad\Gendpoints\Entity\Comment
 *
 * @ORM\Entity
 * @ORM\Table(name="ganner_comment")
 **/
class Comment extends GenericEntity implements GenericEntityHandler
{
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue * */
    private $id;

    /** @ORM\Column(type="datetime") * */
    private $created;

    /** @ORM\Column(type="datetime") * */
    private $modified;

    /** @ORM\Column(type="text") * */
    private $content;

    /** @ORM\Column(type="integer") * */
    private $task_id;

    /** @ORM\Column(type="integer") * */
    private $owner_id;

    /**
     * Project constructor.
     * @param array $aData
     */
    public function __construct(array $aData = [])
    {
        parent::__construct($aData);
        parent::applyData($aData);
        $this->setCreated();
        $this->setModified();
    }

    public function update(array $aData = [])
    {
        global $oGAEntityManager;
        parent::applyData($aData);
        $oGAEntityManager->persist($this);
        $oGAEntityManager->flush();
    }

    /**
     * Returns entity as array
     *
     * @param string|null $sClassName
     * @return array
     */
    public function toArray(string $sClassName = NULL): array
    {
        return parent::toArray(__CLASS__);
    }

    /**
     * @return mixed
     */
    public function remove()
    {
        //@TODO implement time restrictions logic
        global $oGAEntityManager;
        $deleted_id = $this->getId();
        $oGAEntityManager->remove($this);
        $oGAEntityManager->flush();
        return $deleted_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     */
    private function setCreated(): void
    {
        $this->created = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     */
    public function setModified(): void
    {
        $this->modified = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task_id;
    }

    /**
     * @param mixed $task_id
     */
    public function setTask($task_id): void
    {
        $this->task_id = (int)$task_id;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner_id;
    }

    /**
     * @param mixed $owner_id
     */
    public function setOwner($owner_id): void
    {
        $this->owner_id = (int)$owner_id;
    }

    /**
     * Returns class name
     *
     * @return string
     */
    public function getClassName(): string
    {
        return self::class;
    }
}