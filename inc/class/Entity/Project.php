<?php

namespace Gaad\Ganner\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gaad\Ganer\Interfaces\GenericEntityHandler;

/**
 * Gaad\Gendpoints\Entity\Project
 *
 * @ORM\Entity
 * @ORM\Table(name="ganner_project")
 **/
class Project extends GenericEntity implements GenericEntityHandler
{
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue * */
    private $id;

    /** @ORM\Column(type="datetime") * */
    private $created;

    /** @ORM\Column(type="integer") * */
    private $owner_id;

    /** @ORM\Column(length=150) * */
    private $name;

    /** @ORM\Column(type="string", columnDefinition="ENUM('active', 'blocked', 'trashed') NOT NULL DEFAULT 'active'") */
    private $status = 'active';

    /** @ORM\Column(type="integer") * */
    private $ord;


    /**
     * Project constructor.
     * @param array $aData
     */
    public function __construct(array $aData = [])
    {
        parent::__construct($aData);
        parent::applyData($aData);
        $this->setCreated();
        is_null($this->getOrd()) ? $this->setMaxOrder() : false;
    }

    /**
     * @return mixed
     */
    public function removeTask(int $iTaskId)
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $oTask = $oTasksRepo->findBy(["id" => $iTaskId, "project_id" => $this->getId()]);
        if (!empty($oTask)) {
            $oTask[0]->trash();
        }
    }

    /**
     * @return mixed
     */
    public function remove()
    {
        global $oGAEntityManager;
        $deleted_id = $this->getId();
        $this->removeBuckets();
        $this->trash();

        return $deleted_id;
    }

    public function trash()
    {
        $this->setStatus('trashed');
        $this->update();
    }

    /**
     * Returns entity as array
     *
     * @param string|null $sClassName
     * @return array
     */
    public function toArray(string $sClassName = NULL): array
    {
        return parent::toArray(__CLASS__);
    }

    public function update(array $aData = [])
    {
        global $oGAEntityManager;
        parent::applyData($aData);
        $oGAEntityManager->persist($this);
        $oGAEntityManager->flush();
    }

    /**
     * Add existing bucket to project
     *
     * @param int $iBucketId
     * @return bool
     */
    public function addBucket(int $iBucketId): bool
    {
        global $oGAEntityManager;
        $oBucketsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
        $aBuckets = $oBucketsRepo->findBy(["id" => $iBucketId], null, 1, 0);
        if (!empty($aBuckets)) {
            $aBuckets[0]->setProject($this->getId());
            $aBuckets[0]->update();
            return true;
        }
        return false;
    }

    /**
     * Add existing bucket to project
     *
     * @param int $iBucketId
     * @return bool
     */
    public function addTask(int $iTaskId, int $iBucketId = NULL): bool
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["id" => $iTaskId], null, 1, 0);
        if (!empty($aTasks)) {
            $aTasks[0]->setProject($this->getId());
            $aTasks[0]->setParentTask(null);
            !is_null($iBucketId) ? $aTasks[0]->setBucket($iBucketId) : false;
            $aTasks[0]->update();
            return true;
        }
        return false;
    }

    public function removeBuckets()
    {
        global $oGAEntityManager;
        $oBucketsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
        foreach ($oBucketsRepo->findBy(["project_id" => $this->getId()]) as $i => $bucket) {
            $bucket->remove();
        }
    }

    public function removeBucket(int $iBucketId, int $iSentToTargetBucket = NULL)
    {
        global $oGAEntityManager;
        $oBucketsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
        $oBucket = $oBucketsRepo->findBy(["id" => $iBucketId, "project_id" => $this->getId()]);
        if (!empty($oBucket)) {
            $oBucket[0]->remove(true, $iSentToTargetBucket);
        }

    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param mixed $created
     */
    private function setCreated(): void
    {
        $this->created = new \DateTime();
    }

    /**
     * @param mixed $owner_id
     */
    public function setOwner($owner_id): void
    {
        $this->owner_id = (int)$owner_id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = (string)$name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getOrd()
    {
        return $this->ord;
    }

    /**
     * @param int $ord
     */
    public function setOrd($ord): void
    {
        $this->ord = (int)$ord;
    }

    /**
     * Returns class name
     *
     * @return string
     */
    public function getClassName(): string
    {
        return self::class;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }


}