<?php

namespace Gaad\Ganner\Entity;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping as ORM;
use Gaad\Ganer\Interfaces\GenericEntityHandler;
use ReflectionClass;

/**
 * Gaad\Gendpoints\Entity\Task
 *
 * @ORM\Entity
 * @ORM\Table(name="ganner_task")
 **/
class Task extends GenericEntity implements GenericEntityHandler
{
    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue * */
    private $id;

    /** @ORM\Column(type="integer") * */
    private $project_id;

    /** @ORM\Column(type="integer", nullable=true) * */
    private $parent_task_id;

    /** @ORM\Column(type="integer") * */
    private $owner_id;

    /** @ORM\Column(type="integer", nullable=true) * */
    private $contractor_id;

    /** @ORM\Column(type="integer", nullable=true) * */
    private $bucket_id;

    /** @ORM\Column(type="datetime") * */
    private $created;

    /** @ORM\Column(type="datetime") * */
    private $modified;

    /** @ORM\Column(length=150, options={"default":"Untitled task"}) * */
    private $name = 'Untitled task';

    /** @ORM\Column(type="text", nullable=true) * */
    private $text;

    /** @ORM\Column(type="string", columnDefinition="ENUM('task', 'bug') NOT NULL DEFAULT 'task'") */
    private $type = 'task';

    /** @ORM\Column(type="string", columnDefinition="ENUM('active', 'blocked', 'trashed', 'archive') NOT NULL DEFAULT 'active'") */
    private $status = 'active';

    /** @ORM\Column(type="boolean", options={"default":false}) * */
    private $urgent = false;

    /** @ORM\Column(type="boolean", options={"default":false}) * */
    private $hidden = false;

    /** @ORM\Column(type="integer") * */
    private $ord;

    /**
     * Project constructor.
     * @param array $aData
     */
    public function __construct(array $aData = [])
    {
        parent::__construct($aData);
        parent::applyData($aData);
        $this->setDefaults();
    }

    /**
     *
     */
    public function removeComments()
    {
        global $oGAEntityManager;
        $oCommentsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Comment");
        foreach ($oCommentsRepo->findAll() as $i => $comment) {
            $comment->remove();
        }
    }

    public function update(array $aData = [])
    {
        global $oGAEntityManager;
        !empty($aData) ? parent::applyData($aData) : false;
        $oGAEntityManager->persist($this);
        $oGAEntityManager->flush();
    }

    public function addContractor(int $iContractorId = NULL)
    {
        $this->setContractor($iContractorId);
        $this->update();
    }

    public function moveToBucket(int $iBucketId = NULL)
    {
        $this->setBucket($iBucketId);
        $this->update();
    }

    public function hide()
    {
        $this->setHidden(true);
        $this->update();
    }

    public function show()
    {
        $this->setHidden(false);
        $this->update();
    }

    public function moveToProject(int $iProjectId)
    {
        $this->setProject($iProjectId);
        $this->update();
    }

    public function moveAsSubTask(int $iParentTaskId)
    {
        $this->setParentTask($iParentTaskId);
        $this->update();
    }

    public function trash()
    {
        $this->setStatus('trashed');
        $this->update();
    }

    /**
     * Returns entity as array
     *
     * @param string|null $sClassName
     * @return array
     */
    public function toArray(string $sClassName = NULL): array
    {
        return parent::toArray(__CLASS__);
    }

    public function removeComment(int $iCommentId)
    {
        global $oGAEntityManager;
        $oCommentsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Comment");
        $oComment = $oCommentsRepo->findBy(["id" => $iCommentId]);
        if (!empty($oComment)) {
            return $oComment[0]->remove();
        }
    }

    public function removeChildTask(int $iTaskId)
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $oTask = $oTasksRepo->findBy(["id" => $iTaskId]);
        if (!empty($oTask)) {
            $oTask[0]->setParentTask(null);
            $oTask[0]->update();
        }
    }

    public function addChildTask(int $iTaskId)
    {
        global $oGAEntityManager;
        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $oTask = $oTasksRepo->findBy(["id" => $iTaskId]);
        if (!empty($oTask)) {
            $oTask[0]->setParentTask($this->getId());
            $oTask[0]->update();
        }
    }

    /**
     * @return mixed
     */
    public function remove()
    {
        global $oGAEntityManager;
        $deleted_id = $this->getId();
        $this->removeComments();

        $oGAEntityManager->remove($this);
        $oGAEntityManager->flush();

        return $deleted_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    private function setCreated(): void
    {
        $this->created = new \DateTime();
    }

    /**
     * @param mixed $created
     */
    private function setModified(): void
    {
        $this->modified = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getBucket()
    {
        return $this->bucket_id;
    }

    /**
     * @param mixed $bucket_id
     */
    public function setBucket($bucket_id): void
    {
        $this->bucket_id = $bucket_id;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner_id;
    }

    /**
     * @param mixed $owner_id
     */
    public function setOwner($owner_id): void
    {
        $this->owner_id = (int)$owner_id;
    }

    /**
     * @return mixed
     */
    public function getContractor()
    {
        return $this->contractor_id;
    }

    /**
     * @param mixed $contractor_id
     */
    public function setContractor(int $contractor_id = NULL): void
    {
        $this->contractor_id = !is_null($contractor_id) ? (int)$contractor_id : null;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getOrd()
    {
        return $this->ord;
    }

    /**
     * @param mixed $ord
     */
    public function setOrd($ord): void
    {
        $this->ord = $ord;
    }

    /**
     * Returns class name
     *
     * @return string
     */
    public function getClassName(): string
    {
        return self::class;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project_id;
    }

    /**
     * @param mixed $project_id
     */
    public function setProject($project_id): void
    {
        $this->project_id = (int)$project_id;
    }

    /**
     * @return mixed
     */
    public function getParentTask()
    {
        return $this->parent_task_id;
    }

    /**
     * @param mixed $parent_task_id
     */
    public function setParentTask($parent_task_id): void
    {
        $this->parent_task_id = !is_null($parent_task_id) ? (int)$parent_task_id : null;
    }

    /**
     * @TODO Implement with entity specific logic
     */
    public function setMaxOrder()
    {
        if (method_exists($this, "setOrd")) {
            global $oGAEntityManager;
            $query = $oGAEntityManager->createQuery("SELECT max(b.ord) as maxOrder FROM {$this->getClassName()} b");
            $a = $query->getResult();
            $this->setOrd((int)$a[0]['maxOrder'] + 1);
        }
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUrgent()
    {
        return $this->urgent;
    }

    /**
     * @param mixed $urgent
     */
    public function setUrgent($urgent): void
    {
        $this->urgent = $urgent;
    }

    /**
     * @return mixed
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * @param mixed $hidden
     */
    public function setHidden($hidden): void
    {
        $this->hidden = $hidden;
    }


    public function setDefaults()
    {
        $this->setCreated();
        $this->setModified();
        is_null($this->getOrd()) ? $this->setMaxOrder() : false;
    }

}