<?php

namespace Gaad\Ganner\Entity;


use Gaad\Gendpoints\GEndpoint;

class GenericEntity
{

    function __construct(array $aData = [])
    {

    }


    /**
     * Return entity as a array
     *
     * @param string|null $sClassName
     * @return array
     */
    public function toArray(string $sClassName = NULL): array
    {
        $aArray = [];
        $sClassName = str_replace(["\0", ""], [""], array_reverse(explode("\\", $sClassName))[0]);
        foreach ((array)$this as $k => $v) {
            $k = str_replace(["\0", $sClassName], ["", ""], array_reverse(explode("\\", (string)$k))[0]);
            $aArray[$k] = is_object($v) ? (array)$v : $v;
        }
        return $aArray;
    }

    /**
     * Passed array contains data. When index is matched to class property, value is set
     *
     * @param array $aData
     */
    function applyData(array $aData = [])
    {
        if (!empty($aData)) {
            foreach ($aData as $k => $v) {

                if ($v === GEndpoint::HEADER_MISSING) continue;

                if (method_exists($this, "set" . $k)) {
                    call_user_func([$this, "set" . $k], $v);
                }
            }
        }
    }

    public function setMaxOrder()
    {
        //@TODO refactor this method, implement OrderManager
        if (method_exists($this, "setOrd")) {
            global $oGAEntityManager;
            $query = $oGAEntityManager->createQuery("SELECT max(b.ord) as maxOrder FROM {$this->getClassName()} b");
            $a = $query->getResult();
            $this->setOrd((int)$a[0]['maxOrder'] + 1);
        }
    }

}