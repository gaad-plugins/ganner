<?php
/**
 * https://www.doctrine-project.org/projects/doctrine-collections/en/1.6/expressions.html
 */

namespace Gaad\Ganner\Handlers;


use Doctrine\Common\Collections\Criteria as Criteria;
use PHPUnit\Exception;

class EntityListWithCriteria
{
    private $aHeaders = [];
    private $sEntity = "";
    private $oRepo;
    private $oCriteria;
    private $aCriteria;
    private $iPage;
    private $iPerPage;
    private $aResult = [];
    private $sOrder = 'id';
    private $sOrderBy = Criteria::ASC;

    /**
     * EntityListParams constructor.
     * @param string $sEntity
     * @param array $aHeaders
     */
    public function __construct(string $sEntity, array $aHeaders)
    {
        $this->setAHeaders($aHeaders);
        $this->setSOrder($aHeaders['Order']);
        $this->setSOrderBy($aHeaders['Orderby']);
        $this->setIPage($aHeaders['Page']);
        $this->setIPerPage($aHeaders['Perpage']);
        $this->setSEntity($sEntity);
        $this->setORepo();
        $this->setOCriteria();
        $this->setACriteria();
//https://stackoverflow.com/questions/14786937/how-to-use-a-findby-method-with-comparative-criteria
    }

    /**
     * @param mixed $oRepo
     */
    private function setORepo(): void
    {
        global $oGEEntityManager;
        $this->oRepo = $oGEEntityManager->getRepository($this->getSEntity());

    }

    /**
     * @return array
     */
    public function getSEntity(): string
    {
        return $this->sEntity;
    }

    /**
     * @param array $sEntity
     */
    public function setSEntity(string $sEntity): void
    {
        $this->sEntity = $sEntity;
    }

    /**
     * Returns max pages
     * @return int
     */
    public function getMaxPages()
    {
        $totalRecords = $this->getORepo()->matching($this->getOCriteria())->count();
        $additionalPage = $totalRecords % $this->getIPerPage() > 0 ? 1 : 0;
        return (int)($totalRecords / $this->getIPerPage()) + $additionalPage;
    }

    public function toArray()
    {
        $oMatched = $this->paginate($this->getORepo()->matching($this->getOCriteria()));
        $aMatched = [];
        if (!empty($oMatched)) {
            foreach ($oMatched as $k => $v) {
                $aMatched[] = $v->toArray();
            }
        }
        return $aMatched;
    }

    /**
     * @return mixed
     */
    public function getORepo()
    {
        return $this->oRepo;
    }

    /**
     * Returns Criteria object with all requested and default criteria set
     *
     * @return mixed
     */
    public function getOCriteria()
    {
        $oCriteria = $this->oCriteria instanceof Criteria ? $this->oCriteria : new Criteria();
        if (!empty($this->getACriteria())) {
            $this->applyCriteria();
        }

        $oCriteria->orderBy([$this->getSOrderBy() => $this->getSOrder()]);

        return $oCriteria;
    }

    /**
     * @param mixed $oCriteria
     */
    public function setOCriteria(Criteria $oCriteria = NULL): void
    {
        $this->oCriteria = is_null($oCriteria) ? new Criteria() : $oCriteria;
    }

    public function getSelector()
    {
        return 'findBy';
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return $this->aHeaders;
    }

    /**
     * @param array $aHeaders
     */
    public function setAHeaders(array $aHeaders): void
    {
        $this->aHeaders = $aHeaders;
    }

    /**
     * @return array
     */
    public function getAResult(): array
    {
        return $this->aResult;
    }

    /**
     * @param array $aResult
     */
    public function setAResult(array $aResult): void
    {
        $this->aResult = $aResult;
    }

    /**
     * Get pagination parameters, return sliced results array
     * @param $matching
     */
    private function paginate($oMatched)
    {
        $page = $this->getIPage() === 1 ? 0 : $this->getIPage() - 1;
        $iOffset = $page * $this->getIPerPage();
        return $oMatched->slice($iOffset, $this->getIPerPage());
    }

    /**
     * @return mixed
     */
    public function getIPage()
    {
        return $this->iPage;
    }

    /**
     * @param mixed $iPage
     */
    public function setIPage($iPage): void
    {
        $this->iPage = (int)$iPage <= 0 ? 1 : (int)$iPage;
    }

    /**
     * @return mixed
     */
    public function getIPerPage()
    {
        return $this->iPerPage;
    }

    /**
     * @param mixed $iPerPage
     */
    public function setIPerPage($iPerPage): void
    {
        if (is_null($iPerPage)) {
            global $geConfig;
            $this->iPerPage = $geConfig->get('itemsPerPage', 'ganner');
        } else
            $this->iPerPage = (int)$iPerPage <= 0 ? 1 : (int)$iPerPage;
    }

    /**
     * @return mixed
     */
    public function getACriteria()
    {
        return $this->aCriteria;
    }

    private function setACriteria(): void
    {
        $aCriteria = [];
        foreach ($this->aHeaders as $header => $v) {
            $matches = [];
            preg_match_all('/^Criteria-?(or|and)?-(\d)$/mi', $header, $matches, PREG_SET_ORDER, 0);
            if (!empty($matches)) {
                $index = (int)array_pop($matches[0]);
                $aCriteria[$index] = [
                    "headerName" => $header,
                    "operator" => $matches[0][1],
                    "value" => $v,
                ];
            }
        }
        $this->aCriteria = $aCriteria;
    }

    /**
     * Creates Doctrine Criteria from passed request headers
     */
    private function applyCriteria(): void
    {
        $tmpComp = null;
        $oCriteria = $this->oCriteria;
        $aCriteria = $this->getACriteria();
        foreach ($aCriteria as $k => $v) {
            $sCondition = stripcslashes($v['value']);
            $whereMethodName = strtolower($v['operator']);
            $whereMethodName .= $whereMethodName ? "Where" : "where";
            try {
                eval('$tmpComp = $oCriteria->expr()->' . $this->parseCondition($sCondition) . ';');
            } catch (Exception $e) {
                //@TODO log something here
                continue;
            }
            if ($tmpComp) $oCriteria->$whereMethodName($tmpComp);
            unset($tmpComp);
        }
    }

    /**
     * Check if passed condition doesn't contain any structured data.
     * Use object creator in place of passed value if detects one of listed types.
     * Example: gt('created', '2020-03-18 06:34:40') will be parsed to gt('created', new Datetime('2020-03-18 06:34:40'))
     *
     * @param string $sCondition
     * @return string|string[]
     */
    private function parseCondition(string $sCondition)
    {
        $regExpMap = [
            'Datetime' => '/\'(\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])+ (00|[0-9]|1[0-9]|2[0-3])+:([0-9]|[0-5][0-9])+:([0-9]|[0-5][0-9])+)\'/m'
        ];

        foreach ($regExpMap as $type => $regExp) {
            preg_match_all($regExp, $sCondition, $matches, PREG_SET_ORDER, 0);
            $testCasting = null;
            if ($matches) {
                //check if passed condition doesn't contain an object instantiation already
                //example: passed header looks like: gt('created', new Datetime('2020-03-18 06:34:40'))
                if (strpos($sCondition, $type)) {
                    return $sCondition;
                }

                try {
                    $testCasting = new $type($matches[0][1]);
                } catch (Exception $e) {
                    break;
                }
                if ($testCasting) {
                    $sCasting = "new {$type}({$matches[0][0]})";
                    $parsedCondition = str_replace($matches[0][0], $sCasting, $sCondition);

                    break;
                }
            }
        }
        return $parsedCondition ?? $sCondition;
    }

    /**
     * @return string
     */
    public function getSOrder(): string
    {
        return $this->sOrder;
    }

    /**
     * @param string $sOrder
     */
    public function setSOrder(string $sOrder = NULL): void
    {
        $this->sOrder = $sOrder ?? Criteria::ASC;
    }

    /**
     * @return string
     */
    public function getSOrderBy(): string
    {
        return $this->sOrderBy;
    }

    /**
     * @param string $sOrderBy
     */
    public function setSOrderBy(?string $sOrderBy = NULL): void
    {
        $this->sOrderBy = $sOrderBy ?? 'id';
    }

    private function applyOrderBy()
    {
    }


}