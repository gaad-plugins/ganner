<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class BucketCreateEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Project', '?:Name'];

    public function validate__Project($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oUsersRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aUsers = $oUsersRepo->findBy(["id" => $value], null, 1, 0);
        empty($aUsers) ? $this->reportError(\__("Project doesn't exists"), 444) : false;
    }

    public function validate__Name($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $length = strlen($value);
        $length < 5 ? $this->reportError(\__("name is too short")) : false;
        $length > 50 ? $this->reportError(\__("name is too long")) : false;

        /*        $oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
                $aProjects = $oProjectsRepo->findBy(["name" => $value]);
                !empty($aProjects) ? $this->reportError(\__("name is already in use")) : false;*/
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}