<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class BucketListEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{
    const aHeaders = ["Project"];

    public function validate__Project($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oProjectsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aProjects = $oProjectsRepo->findBy(["id" => $value], null, 1, 0);
        empty($aProjects) ? $this->reportError(\__("Project doesn't exists"), 444) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}