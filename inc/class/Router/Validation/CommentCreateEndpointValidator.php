<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class CommentCreateEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Task', 'Owner', 'Content'];

    public function validate__Task($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oTasksRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["id" => $value], null, 1, 0);
        empty($aTasks) ? $this->reportError(\__("Task doesn't exists"), 447) : false;
    }

    public function validate__Owner($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oUsersRepo = $oGEEntityManager->getRepository("\Gaad\Gendpoints\Entity\User");
        $aUsers = $oUsersRepo->findBy(["ID" => $value], null, 1, 0);
        empty($aUsers) ? $this->reportError(\__("Owner doesn't exists"), 448) : false;
    }

    public function validate__Content($value, array $aHeaders): void
    {
        strlen($value) === 0 ? $this->reportError(\__("Content cannot be empty")) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}