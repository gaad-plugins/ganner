<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class CommentListEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{
    const aHeaders = [];

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}