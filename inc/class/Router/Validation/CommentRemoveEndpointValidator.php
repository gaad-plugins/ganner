<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class CommentRemoveEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id'];

    public function validate__Id($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 522);
            return;
        }

        $oCommentsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Comment");
        $aComments = $oCommentsRepo->findBy(["id" => $value]);
        empty($aComments) ? $this->reportError(\__("Comment doesn't exists"), 451) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}