<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class ProjectAddBucketEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id', 'Project'];

    public function validate__Id($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 523);
            return;
        }

        $oBucketsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
        $aBuckets = $oBucketsRepo->findBy(["id" => $value], null, 1, 0);
        empty($aBuckets) ? $this->reportError(\__("Bucket doesn't exists"), 444) : false;
    }

    public function validate__Project($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oProjectsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aProjects = $oProjectsRepo->findBy(["id" => $value], null, 1, 0);
        empty($aProjects) ? $this->reportError(\__("Project doesn't exists"), 444) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}