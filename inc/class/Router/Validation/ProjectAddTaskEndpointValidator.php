<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class ProjectAddTaskEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id', 'Target', '?:Bucket'];

    public function validate__Id($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 523);
            return;
        }

        $oTasksRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["id" => $value]);
        empty($aTasks) ? $this->reportError(\__("Task doesn't exists"), 452) : false;
    }

    public function validate__Bucket($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 523);
            return;
        }

        $oBucketsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
        $aBuckets = $oBucketsRepo->findBy(["id" => $value]);
        empty($aBuckets) ? $this->reportError(\__("Bucket doesn't exists"), 452) : false;
    }

    public function validate__Target($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 523);
            return;
        }
        $oProjectsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aProjects = $oProjectsRepo->findBy(["id" => $value], null, 1, 0);
        empty($aProjects) ? $this->reportError(\__("Project doesn't exists"), 444) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}