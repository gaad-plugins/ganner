<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class ProjectCreateEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{


    const aHeaders = ['Name', 'Owner'];

    public function validate__Owner($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oUsersRepo = $oGEEntityManager->getRepository("\Gaad\Gendpoints\Entity\User");
        $aUsers = $oUsersRepo->findBy(["ID" => $value], null, 1, 0);
        empty($aUsers) ? $this->reportError(\__("Owner doesn't exists")) : false;
    }

    public function validate__Name($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $length = strlen($value);
        $length < 5 ? $this->reportError(\__("name is too short")) : false;
        $length > 50 ? $this->reportError(\__("name is too long")) : false;

        $oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aProjects = $oProjectsRepo->findBy(["name" => $value]);
        !empty($aProjects) ? $this->reportError(\__("name is already in use")) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}