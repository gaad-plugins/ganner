<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class ProjectRemoveEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id'];

    public function validate__Id($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"));
            return;
        }

        $oProjectsRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aProjects = $oProjectsRepo->findBy(["id" => $value]);
        empty($aProjects) ? $this->reportError(\__("Project doesn't exists"), 444) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}