<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class ProjectUpdateEndpointValidator extends ProjectCreateEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id', "?:Owner", "?:Name"];
    private $iMinHeaders = 2;


    public function validate__Name($value, array $aHeaders): void
    {
        global $oGEEntityManager;

        $oProjectsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aProjects = $oProjectsRepo->findBy(["name" => $value]);
        !empty($aProjects) ? $this->reportError(\__("New name and current are the same, update failed"), 452) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }

    /**
     * @return int
     */
    public function getIMinHeaders(): int
    {
        return $this->iMinHeaders;
    }


}