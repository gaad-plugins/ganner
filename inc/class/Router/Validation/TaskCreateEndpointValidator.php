<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Doctrine\Common\Annotations\AnnotationReader;
use Gaad\Ganner\Entity\Task;
use Gaad\Gendpoints\Interfaces\RouteValidationHandler;
use ReflectionClass;

class TaskCreateEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['?:Name', 'Project', "Owner", "?:Parenttask", "?:Contractor", "?:Content", "?:Type", '?:Bucket'];

    public function validate__Bucket($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oBucketsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Bucket");
        $aBuckets = $oBucketsRepo->findBy(["id" => $value], null, 1, 0);
        empty($aBuckets) && !($value === "backlog" || $value === 0 || $value === "0" || $value === null) ? $this->reportError(\__("Bucket doesn't exists"), 446) : false;
    }

    public function validate__Parenttask($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oTasksRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["id" => $value], null, 1, 0);
        empty($aTasks) ? $this->reportError(\__("Task doesn't exists"), 442) : false;
        (int)$aHeaders['Id'] === (int)$value ? $this->reportError(\__("Task can't be a parent to itself"), 342) : false;
    }

    public function validate__Project($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oProjectsRepo = $oGEEntityManager->getRepository("\Gaad\Ganner\Entity\Project");
        $aProjects = $oProjectsRepo->findBy(["id" => $value], null, 1, 0);
        empty($aProjects) ? $this->reportError(\__("Project doesn't exists"), 442) : false;
    }

    public function validate__Owner($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oUsersRepo = $oGEEntityManager->getRepository("\Gaad\Gendpoints\Entity\User");
        $aUsers = $oUsersRepo->findBy(["ID" => $value], null, 1, 0);
        empty($aUsers) ? $this->reportError(\__("Owner doesn't exists")) : false;
    }

    public function validate__Content($value, array $aHeaders): void
    {
        //@TODO implement: check for scripts (security)
    }

    public function validate__Type($value, array $aHeaders): void
    {
        /*$test = new ReflectionClass(Task::class);
        $test->getProperty('table');*/
        $oTypeAnnotation = (new AnnotationReader())->getPropertyAnnotation(
            (new ReflectionClass(Task::class))->getProperty('type'),
            \Doctrine\ORM\Mapping\Column::class
        );
        $sColumnDefinition = [];
        eval('$sColumnDefinition = ' . str_replace("ENUM", "Array", explode(")", $oTypeAnnotation->columnDefinition)[0] . ')') . ';');

        $sColumnDefinition = is_array($sColumnDefinition) ? $sColumnDefinition : [];
        !in_array($value, $sColumnDefinition) ? $this->reportError(\__("Task type error"), 563) : false;
    }

    public function validate__Contractor($value, array $aHeaders): void
    {
        global $oGEEntityManager;
        $oUsersRepo = $oGEEntityManager->getRepository("\Gaad\Gendpoints\Entity\User");
        $aUsers = $oUsersRepo->findBy(["ID" => $value], null, 1, 0);
        empty($aUsers) ? $this->reportError(\__("Contractor doesn't exists")) : false;
    }

    public function validate__Name($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $length = strlen($value);
        $length < 3 ? $this->reportError(\__("name is too short")) : false;
        $length > 250 ? $this->reportError(\__("name is too long")) : false;

        /*$oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["name" => $value, "bucket_id" => $aHeaders['Bucket']]);
        !empty($aTasks) ? $this->reportError(\__("Task name is already in use")) : false;*/
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}