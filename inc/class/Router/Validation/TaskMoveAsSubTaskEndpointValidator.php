<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class TaskMoveAsSubTaskEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id', 'Target'];

    public function validate__Id($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 523);
            return;
        }

        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["id" => $value]);
        empty($aTasks) ? $this->reportError(\__("Task doesn't exists"), 452) : false;
    }

    public function validate__Target($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 634);
            return;
        }

        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["id" => $value]);
        empty($aTasks) ? $this->reportError(\__("Target task doesn't exists"), 482) : false;
        $aHeaders['id'] === $value ? $this->reportError(\__("Task can't be moved as sub-task to itself"), 482) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}