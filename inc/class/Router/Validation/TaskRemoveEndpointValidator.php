<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class TaskRemoveEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id'];

    public function validate__Id($value, array $aHeaders): void
    {
        global $oGAEntityManager;
        $value = (int)$value;
        if ($value === 0) {
            $this->reportError(\__("Id should be integer"), 523);
            return;
        }

        $oTasksRepo = $oGAEntityManager->getRepository("\Gaad\Ganner\Entity\Task");
        $aTasks = $oTasksRepo->findBy(["id" => $value]);
        empty($aTasks) ? $this->reportError(\__("Task doesn't exists"), 452) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}