<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class TaskUpdateEndpointValidator extends TaskCreateEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Id', "?:Bucket", "?:Owner", "?:Name", "?:Contractor", "?:Type", "?:Order", "?:Content", "?:Project", "?:Parenttask"];
    private $iMinHeaders = 2;

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }

    /**
     * @return int
     */
    public function getIMinHeaders(): int
    {
        return $this->iMinHeaders;
    }


}