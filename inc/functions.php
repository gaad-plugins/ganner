<?php

namespace Gaad\Gunner\Core\Filters;

use Gaad\Gendpoints\Config\Config;

if (!function_exists('\Gaad\Gunner\Core\Filters\userRolesAdd')) {
    function userRolesAdd()
    {
        global $geConfig;
        $aRolesYaml = $geConfig->get('roles-yaml', 'wp-roles');
        $oGannerWPRoles = new Config(basename($aRolesYaml), [__GANNER_DIR__ . "/config"]);

        foreach ($oGannerWPRoles->get() as $slug => $roleMeta) {
            remove_role($slug);//for dev
            $role = add_role($slug, __($roleMeta['name'], 'ganner'),
                [
                    'read' => true, // true allows this capability
                    'edit_posts' => true, // Allows user to edit their own posts
                    'edit_pages' => false, // Allows user to edit pages
                    'edit_others_posts' => false, // Allows user to edit others posts not just their own
                    'create_posts' => false, // Allows user to create new posts
                    'manage_categories' => false, // Allows user to manage post categories
                    'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
                    'edit_themes' => false, // false denies this capability. User can’t edit your theme
                    'install_plugins' => false, // User cant add new plugins
                    'update_plugin' => false, // User can’t update any plugins
                    'update_core' => false // user cant perform core updates
                ]
            );
            if ($role instanceof \WP_Role) {
                $role->add_cap('manage_options', true);
                if (array_key_exists('caps', $roleMeta) && !empty($roleMeta['caps'])) {
                    foreach ($roleMeta['caps'] as $capName) {
                        $role->add_cap($capName, true);
                    }
                }
            }
        }
    }
}

if (!function_exists('\Gaad\Gunner\Core\Filters\configDirectories')) {
    function configDirectories($input)
    {
        $input[] = __GANNER_DIR__ . '/config';
        return $input;
    }
}

if (!function_exists('\Gaad\Gunner\Core\Filters\endpointsDirectories')) {
    function endpointsDirectories($input)
    {
        $input[] = __GANNER_DIR__ . '/gendpoints';
        return $input;
    }
}

