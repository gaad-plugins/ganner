<?php

use Doctrine\Common\Annotations\AnnotationReader;
use Gaad\Ganner\Entity\Project;
use Gaad\Ganner\Entity\Task;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class ApiTester extends \Codeception\Actor
{

    use _generated\ApiTesterActions;

    const SUCCESS_STATUS = 'success';
    const ERROR_STATUS = 'error';
    const TRASHED_STATUS = 'trashed';

    const apiUserLogin = 'apitester1';
    const apiUserPassword = '123';
    const apiUserId = 2;

    const projectTableName = "ganner_project";
    const bucketTableName = "ganner_bucket";
    const taskTableName = "ganner_task";
    const commentTableName = "ganner_comment";

    public function responseError(int $iCode = NULL)
    {
        $this->seeResponseContainsJson(['status' => $this::ERROR_STATUS]);
        if ($iCode) {
            //@TODO implement error codes testing
            die("implement error code");
        }
    }

    public function createProject($sProjectName)
    {
        $this->haveHttpHeader('Owner', $this::apiUserId);
        $this->haveHttpHeader('Name', $sProjectName);
        $this->sendPOST('/ganner/project/create');
        $this->responseSuccess();

        $this->seeInDatabase($this::projectTableName, ['name' => $sProjectName, 'owner_id' => $this::apiUserId]);
    }

    public function responseSuccess()
    {
        $this->seeResponseContainsJson(['status' => $this::SUCCESS_STATUS]);
    }

    public function removeProject($iProjectId)
    {
        $this->haveHttpHeader('Id', $iProjectId);
        $this->sendPOST('/ganner/project/remove');

        $this->responseSuccess();
        $this->seeResponseContainsJson(['data' => ['removed_id' => $iProjectId]]);
        $this->seeInDatabase($this::projectTableName, ['id' => $iProjectId, 'status' => $this::TRASHED_STATUS]);
    }

    public function createBucket(int $iProjectId, string $sBucketName = NULL)
    {
        $this->haveHttpHeader('Project', $iProjectId);
        if (!is_null($sBucketName)) $this->haveHttpHeader('Name', $sBucketName);
        $this->sendPOST('/ganner/bucket/create');

        $this->responseSuccess();
        $iBucketId = $this->grabFromDatabase($this::bucketTableName, 'id', ["name" => $sBucketName]);
        $this->seeResponseContainsJson(['data' => ['created_id' => (string)$iBucketId]]);
        return $iBucketId;
    }

    public function createTask(int $iProjectId, int $ownerId, string $sTaskName, int $sBucketId = NULL)
    {
        $this->haveHttpHeader('Project', $iProjectId);
        $this->haveHttpHeader('Owner', $ownerId);
        $this->haveHttpHeader('Bucket', $sBucketId);
        $this->haveHttpHeader('Name', $sTaskName);
        $this->sendPOST('/ganner/task/create');

        $this->responseSuccess();
        $iTaskId = $this->grabFromDatabase($this::taskTableName, 'id', ["name" => $sTaskName]);
        $this->seeResponseContainsJson(['data' => ['created_id' => (string)$iTaskId]]);
        return $iTaskId;
    }

    public
    function bearerAuth()
    {
        $this->haveHttpHeader('U', $this::apiUserLogin);
        $this->haveHttpHeader('P', $this::apiUserPassword);
        $this->sendPOST('/auth/access-token');
        $this->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $this->seeResponseIsJson();
        $sResponse = $this->grabResponse();
        if ($sResponse) {
            try {
                $aResponse = json_decode($sResponse, true);
                if ($aResponse) {
                    $this->amBearerAuthenticated($aResponse['data']['access_token']);
                    return $aResponse['data']['access_token'];
                }
            } catch (Exception $e) {
            }
        }
        return "";
    }
}
