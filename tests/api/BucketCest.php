<?php

use Helper\Api;

class BucketCest
{
    const testProjectName = "Project for testing Bucket";
    const changedString = "das543ad353f3f0dff12d32s54ad54sa";
    const textBucketName = "test bucket";
    const bucketsMax = 2;
    /**
     * @var Helper\Api
     */
    protected $api;
    private $iProjectId;
    private $iRemoveBucketId;
    private $sBearer;

    public function _inject(Api $Api)
    {
        $this->api = $Api;
    }

    function beforeAllTests(ApiTester $I)
    {
        $this->api->removeAllEntities();
        $I->createProject($this::testProjectName);

        $this->iProjectId = (int)$I->grabFromDatabase($I::projectTableName, 'id', ['name like' => $this::testProjectName]);
    }

    /**
     * Run before each test
     * @param ApiTester $I
     */
    function _before(ApiTester $I)
    {
        !$this->sBearer ? $this->sBearer = $I->bearerAuth() : $I->amBearerAuthenticated($this->sBearer);
    }

    // tests
    public function createNamedBucket(ApiTester $I)
    {
        $I->wantToTest("Bucket can be created in a Project");

        $I->createBucket($this->iProjectId, $this::textBucketName);

        for ($i = 0; $i < $this::bucketsMax; $i++) {
            $sBucketName = $this::textBucketName . " " . $i;
            $iBucketId = $I->createBucket($this->iProjectId, $sBucketName);
            $this->iRemoveBucketId = (int)$iBucketId;
        }
    }

    /**
     * @param ApiTester $I
     * @depend createNamedBucket
     */
    public function listBuckets(ApiTester $I)
    {
        $I->wantToTest("Buckets can be listed");
        $I->haveHttpHeader('Project', $this->iProjectId);
        $I->sendPOST('/ganner/bucket/list');

        $I->responseSuccess();
        //@TODO make test more accurate
        $I->seeResponseContainsJson(['data' => ['length' => $this::bucketsMax + 1]]);
    }

    /**
     * @param ApiTester $I
     * @depends createNamedBucket
     */
    public function removeBucket(ApiTester $I)
    {
        $I->wantToTest("Bucket can be removed");
        $I->haveHttpHeader('Id', $this->iRemoveBucketId);
        $I->sendPOST('/ganner/bucket/remove');

        $I->responseSuccess();
        $I->seeResponseContainsJson(['data' => ['removed_id' => $this->iRemoveBucketId]]);
        $I->seeInDatabase($I::bucketTableName, ['id' => $this->iRemoveBucketId, 'status' => $I::TRASHED_STATUS]);
    }

}
