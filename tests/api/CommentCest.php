<?php

use Helper\Api;

class CommentCest
{
    const testProjectName = "Project for testing Comments";
    const changedString = "das543ad353f3f0dff12d32s54ad54sa";

    const textTaskName = "test task 1";
    const ownerId = 3;
    const commentContent = [
        0 => "comment 1",
        1 => "comment 2",
        2 => "comment 3",
    ];

    /**
     * @var Helper\Api
     */
    protected $api;
    private $iProjectId;
    private $sBearer;
    private $aTask;

    public function _inject(Api $Api)
    {
        $this->api = $Api;
    }

    function beforeAllTests(ApiTester $I)
    {
        $this->api->removeAllEntities();
        $I->createProject($this::testProjectName);
        $this->iProjectId = (int)$I->grabFromDatabase($I::projectTableName, 'id', ['name like' => $this::testProjectName]);

        $this->aTask[] = $I->createTask($this->iProjectId, $this::ownerId, $this::textTaskName, null);
    }

    /**
     * Run before each test
     * @param ApiTester $I
     */
    public function _before(ApiTester $I)
    {
        !$this->sBearer ? $this->sBearer = $I->bearerAuth() : $I->amBearerAuthenticated($this->sBearer);
    }

    /**
     * @param ApiTester $I
     */
    public function addComment(ApiTester $I)
    {
        $I->wantToTest("Comment can be added to the Task");
        $I->haveHttpHeader('Content', $this::commentContent[0]);
        $I->haveHttpHeader('Owner', $this::ownerId);
        $I->haveHttpHeader('Task', $this->aTask[0]);
        $I->sendPOST('/ganner/comment/create');

        $I->responseSuccess();
        //$I->seeInDatabase($I::taskTableName, ['id' => $this->backlogTaskId, 'contractor_id' => self::contractorId]);
        //$I->seeResponseContainsJson(['data' => ['updated_id' => $this->backlogTaskId]]);
    }

    /**
     * @param ApiTester $I
     * @depend addComment
     */
    public function listComments(ApiTester $I)
    {
        $I->wantToTest("All Comments for Task can be listed");
        $I->haveHttpHeader('Task', $this->iProjectId);
        $I->sendPOST('/ganner/task/list');

        $I->responseSuccess();
        //@TODO make test more accurate
        $I->seeResponseContainsJson(['data' => ['length' => /*count(self::commentContent)*/ 1]]);
    }

}
