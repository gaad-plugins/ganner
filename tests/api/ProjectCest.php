<?php

use Helper\Api;

class ProjectCest
{
    const testProjectName = "Project for testing Project";
    const changedString = "das543ad353f3f0dff12d32s54ad54sa";
    /**
     * @var Helper\Api
     */
    protected $api;
    private $iProjectId;
    private $sBearer;

    public function _inject(Api $Api)
    {
        $this->api = $Api;
    }

    function beforeAllTests(ApiTester $I)
    {
        $this->api->removeAllEntities();
    }

    function _before(ApiTester $I)
    {
        !$this->sBearer ? $this->sBearer = $I->bearerAuth() : $I->amBearerAuthenticated($this->sBearer);
    }

    /**
     * @param ApiTester $I
     */
    public function createProject(ApiTester $I)
    {
        $I->wantToTest("Project can be created");

        $I->createProject($this::testProjectName);

        $this->iProjectId = (int)$I->grabFromDatabase($I::projectTableName, 'id', ['name like' => $this::testProjectName]);
    }

    /**
     * @param ApiTester $I
     * @depend createProject
     */
    public function listProjects(ApiTester $I)
    {
        $I->wantToTest("Projects can be listed");
        $I->sendPOST('/ganner/project/list');

        $I->responseSuccess();
        //@TODO make test more accurate
        $I->seeResponseContainsJson(['data' => ['length' => 1]]);
    }

    /**
     * @param ApiTester $I
     * @depend createProject
     */
    public function changeProjectsName(ApiTester $I)
    {
        $sUpdatedName = $this::testProjectName . $this::changedString;

        $I->wantToTest("Project update: change Name");

        $I->haveHttpHeader('Id', $this->iProjectId);
        $I->haveHttpHeader('Name', $sUpdatedName);
        $I->sendPOST('/ganner/project/update');

        $I->responseSuccess();
        $I->seeResponseContainsJson(['data' => ['updated_id' => $this->iProjectId]]);
        $I->seeInDatabase($I::projectTableName, ['id' => $this->iProjectId, 'name' => $sUpdatedName]);
    }

    /**
     * @param ApiTester $I
     * @depend createProject
     */
    public function changeNameFail1(ApiTester $I)
    {
        $I->wantToTest("Project update failure: no Name passed");

        $I->haveHttpHeader('Id', $this->iProjectId);
        $I->sendPOST('/ganner/project/update');

        $I->responseError();
    }

    /**
     * @param ApiTester $I
     * @depend createProject
     */
    public function changeNameFail2(ApiTester $I)
    {
        $I->wantToTest("Project update failure: same Name passed");

        $sUpdatedName = $this::testProjectName . $this::changedString;

        $I->haveHttpHeader('Id', $this->iProjectId);
        $I->haveHttpHeader('Name', $sUpdatedName);
        $I->sendPOST('/ganner/project/update');
        $I->grabResponse();

        $I->responseError();
    }

    /**
     * @param ApiTester $I
     * @depend createProject
     */
    public function removeProject(ApiTester $I)
    {
        $I->wantToTest("Project can be removed");

        $I->removeProject($this->iProjectId);

        $I->responseSuccess();
    }
}