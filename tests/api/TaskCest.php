<?php

use Helper\Api;

class TaskCest
{
    const testProjectName = "Project for testing Tasks";
    const changedString = "das543ad353f3f0dff12d32s54ad54sa";

    const textBucketName = "test bucket";
    const bucketsMax = 2;

    const bakclogTaskName = "Backlog task";
    const textTaskName = [
        0 => ["test task 1"],
        1 => ["test task 2"],
        2 => ["test task 3"]
    ];
    const ownerId = 2;
    const contractorId = 3;

    /**
     * @var Helper\Api
     */
    protected $api;
    private $iProjectId;
    private $iRemoveTasktId;
    private $sBearer;
    private $aTask;
    /**
     * @var void
     */
    private $backlogTaskId;

    public function _inject(Api $Api)
    {
        $this->api = $Api;
    }

    function beforeAllTests(ApiTester $I)
    {
        $this->api->removeAllEntities();
        $I->createProject($this::testProjectName);
        $this->iProjectId = (int)$I->grabFromDatabase($I::projectTableName, 'id', ['name like' => $this::testProjectName]);

        for ($i = 0; $i < $this::bucketsMax; $i++) {
            $sBucketName = $this::textBucketName . " " . $i;
            $iBucketId = $I->createBucket($this->iProjectId, $sBucketName);
        }

        $this->aTask[] = $I->createTask($this->iProjectId, $this::ownerId, $this::textTaskName[0][0], $iBucketId);
    }

    /**
     * Run before each test
     * @param ApiTester $I
     */
    public function _before(ApiTester $I)
    {
        !$this->sBearer ? $this->sBearer = $I->bearerAuth() : $I->amBearerAuthenticated($this->sBearer);
    }

    public function addBacklogTask(ApiTester $I)
    {
        $I->wantToTest("Task can be added to the Project (no bucket = backlog)");
        $this->backlogTaskId = $I->createTask($this->iProjectId, self::ownerId, self::bakclogTaskName);
    }


    public function addContractor(ApiTester $I)
    {
        $I->wantToTest("Contractor can be added to the Task");
        $I->haveHttpHeader('Id', $this->backlogTaskId);
        $I->haveHttpHeader('Contractor', self::contractorId);
        $I->sendPOST('/ganner/task/update');

        $I->responseSuccess();
        $I->seeInDatabase($I::taskTableName, ['id' => $this->backlogTaskId, 'contractor_id' => self::contractorId]);
        $I->seeResponseContainsJson(['data' => ['updated_id' => $this->backlogTaskId]]);
    }

    /**
     * @param ApiTester $I
     */
    public function createTasks(ApiTester $I)
    {
        $I->wantToTest("Tasks can be added to the Project->Task");
        $counter = 0;
        foreach (self::textTaskName as $i => $aTask) {
            if ($counter === 0) {
                $counter++;
                continue;
            }
            $this->aTask[] = $I->createTask($this->iProjectId, self::ownerId, $aTask[0]);
            $counter++;
        }

    }

    /**
     * @param ApiTester $I
     * @depend createTasks
     */
    public function listTasks(ApiTester $I)
    {
        $I->wantToTest("All Tasks in a Project can be listed");
        $I->haveHttpHeader('Project', $this->iProjectId);
        $I->haveHttpHeader('Bucket', $this->aTask[0]);
        $I->sendPOST('/ganner/task/list');

        $I->responseSuccess();
        //@TODO make test more accurate
        $I->seeResponseContainsJson(['data' => ['length' => count(self::textTaskName) + 1]]);
    }

}
